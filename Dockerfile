# Usa una imagen base de Ubuntu 20.04
FROM ubuntu:20.04

# Actualiza el sistema y luego instala las herramientas
RUN apt-get update && \
    apt-get install -y curl nano tar xz-utils

# Descarga e instala Node.js v18.18.2
RUN curl -O https://nodejs.org/dist/v18.18.2/node-v18.18.2-linux-x64.tar.xz && \
    mkdir -p /usr/local/lib/nodejs && \
    tar -xJvf node-v18.18.2-linux-x64.tar.xz -C /usr/local/lib/nodejs

# Configura la variable de entorno en el mismo Dockerfile
ENV VERSION=v18.18.2
ENV DISTRO=linux-x64
ENV PATH="/usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin:$PATH"

# Define el comando predeterminado al iniciar el contenedor
CMD ["bash"]
