#!/bin/bash

:'

#Para decargar el sh y ejecutarlo en el servidor 

apt update 
apt install nano curl

curl -o install.sh https://gitlab.com/jose.arriagada1701/proyecto-ingsw/-/raw/main/install.sh
chmod +x install.sh && ./install.sh

'
# Actualizar la lista de paquetes
apt update

# Instalar las librerías necesarias
echo "Instalando librerías"
apt install -y curl nano tar xz-utils git

# Descargar Node.js
echo "Descargando Node.js"
curl -O https://nodejs.org/dist/v18.18.2/node-v18.18.2-linux-x64.tar.xz

# Crear el directorio de instalación de Node.js
mkdir -p /usr/local/lib/nodejs

# Descomprimir Node.js
echo "Descomprimiendo y Instalando Node.js"
tar -xJvf node-v18.18.2-linux-x64.tar.xz -C /usr/local/lib/nodejs

# Configurar las variables de entorno en el archivo ~/.profile
echo -e '\n# Nodejs\nVERSION=v18.18.2\nDISTRO=linux-x64\nexport PATH=/usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin:$PATH' >> ~/.profile

# Recargar el perfil actual para aplicar las configuraciones
. ~/.profile

echo "Node.js instalado"



