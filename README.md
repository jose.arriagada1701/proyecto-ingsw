# Proyecto Ingenería de Sowftware
## Por José Arriagada 2023-02

El proyecto está enmarcado en la transformación de digital del Estado y enfocado en los
Municipios, en esté informe se describe todo lo relacionado con el producto que se debe
entregar para contribuir a está transformación, en concreto se encargó a esté equipo el
control, flujo y personal: esto consiste en saber quien entra y sale de un municipio y luego
poder consultar de manera rápida y confiable quien ha entrado y salido en el pasado.
Para la realización de esté informe de avance se han realizado entrevistas a usuarios de
municipios y a un cliente ficticio que hace de solicitante de esté software. Es un proyecto
ficticio para el ramo de Ingeniería de Software que pretende desarrollar habilidades de
desarrollo de un producto de Software. Se desarrollará en el Stack MERN que es una fusión
de tecnologías para desarrollo en está área.

![Login](Capturas/pantalla_login.png)

![Crear Assitencia](Capturas/pantalla_creación_asistencia.png)

Para más información vease [informe](informe.pdf)
