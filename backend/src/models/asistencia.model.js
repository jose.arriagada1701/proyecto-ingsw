const mongoose = require("mongoose");

const asistenciaSchema = new mongoose.Schema({
  rut: String, 
  nombre: String,
  apellido: String,
  tipo: String, // "entrada" o "salida"
  registros: [
    {
      id : Number,
      fecha: Date,
      entra: String, 
    },
  ],
});

const Asistencia = mongoose.model("Asistencia", asistenciaSchema);


module.exports = Asistencia;

