// Importa el modulo 'express' para crear las rutas
const express = require("express");

/** Controlador de usuarios */
const usuarioController = require("../controllers/user.controller.js");

/** Middlewares de autorización */
const authorizationMiddleware = require("../middlewares/authorization.middleware.js");

/** Middleware de autenticación */
const authenticationMiddleware = require("../middlewares/authentication.middleware.js");

/** Instancia del enrutador */
const router = express.Router();

//validar
const {obtenerPersonas, obtenerPersona, obtenerRegistro, consulta, eliminarPersona, ElimniarRegistro, nuevaAsistencia, nuevaAsistencia2, modificar, tiempoReal } = require("../controllers/asistencia.controller.js");

// Define el middleware de autenticación para todas las rutas
router.use(authenticationMiddleware);

router.get("/tiempoReal", tiempoReal);


router.get("/consulta",consulta);

router.get("/", obtenerPersonas);

router.get("/:id", obtenerPersona);

router.get("/:id/:id_reg", obtenerRegistro);


router.post("/", nuevaAsistencia2);

router.post("/:rut/:nombre/:apellido/:tipo/:entra", nuevaAsistencia);

router.delete("/:id", eliminarPersona)

router.delete("/:id/:id_reg", ElimniarRegistro)

router.put("/", modificar);



// Exporta el enrutador
module.exports = router;

