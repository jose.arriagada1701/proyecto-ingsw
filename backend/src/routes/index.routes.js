"use strict";
const express = require("express");

/** Enrutador de usuarios */
const userRoutes = require("./user.routes.js");

/** Enrutador de autenticación */
const authRoutes = require("./auth.routes.js");

/** Enrutador de asistencia (importa desde asistencia.routes.js) */
const asistencia = require("./asistencia.route.js");

/** Middleware de autenticación */
const authenticationMiddleware = require("../middlewares/authentication.middleware.js");

/** Instancia del enrutador */
const router = express.Router();

// Define las rutas para los usuarios /api/usuarios
router.use("/users", authenticationMiddleware, userRoutes);

// Define las rutas para la asistencia /api/asistencia
router.use("/asistencia", authenticationMiddleware, asistencia);

// Define las rutas para la autenticación /api/auth
router.use("/auth", authRoutes);

// Exporta el enrutador
module.exports = router;
