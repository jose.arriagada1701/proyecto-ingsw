const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com', // Cambia a tu servidor SMTP
  port: 465,
  secure: true,
  auth: {
    user: process.env.MAIL_USERNAME, // Cambia a tu dirección de correo
    pass: process.env.MAIL_PASSWORD, // Cambia a tu contraseña
  },
});

const plantilla = `
<!DOCTYPE html>
<html>
  <head>
    <title>Notificación de ingreso al sistema</title>
    <style>
      body {
        font-family: Arial, sans-serif;
        background-color: #f0f0f0;
        margin: 0;
        padding: 0;
      }
      .container {
        background-color: #fff;
        max-width: 600px;
        margin: 20px auto;
        padding: 20px;
        border-radius: 5px;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
      }
      p {
        margin: 0 0 10px;
      }
      strong {
        color: #0073e6;
      }
      .footer {
        text-align: center;
        color: #888;
        font-size: 12px;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <p>Estimado usuario,</p>
      <p>Le informamos que la persona <strong>[nombre] [apellido]</strong> ha sido ingresada exitosamente al sistema.</p>
      <p>OBS: La respuesta del servidor fue: [respuesta]</p>
      <p>¡Gracias por utilizar nuestro sistema!</p>
    </div>
    <div class="footer">
      &copy; Sistema de Asistencia, Control y Flujo.
    </div>
  </body>
</html>

`;

async function enviarCorreo(nombre, apellido,respuesta) {
    const mensaje = plantilla.replace("[nombre]", nombre).replace("[apellido]", apellido).replace("[respuesta]", respuesta);

    const mailOptions = {
    from: process.env.MAIL_USERNAME, // Remitente
    to: process.env.MAIL_USERNAME, // Destinatario
    subject: 'Notificación de ingreso al sistema',
    html: mensaje,
  };

  // Envía el correo electrónico
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log('Error al enviar el correo:', error);
    } else {
      console.log('Correo enviado:', info.response);
    }
  });
}


module.exports = {

    enviarCorreo,
}