"use strict";

const Asistencia = require("../models/asistencia.model");
const Persona = require("../models/asistencia.model");

const { handleError } = require("../utils/errorHandler");

const mongoose = require('mongoose');



async function getPersonas(id) {
  
  try {
    const persona = await Persona.find();

    if (!persona) {
      return [null, "No hay Registros en el sistema"];
    }

    return [persona, null];
  } catch (error) {
    handleError(error, "persona.service -> deletepersona");
    return [null, error];
  }
}

async function getPersona(id) {
  
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return [null, "id no valido"];
  }

  try {
    const persona = await Persona.findById(id);

    if (!persona) {
      return [null, "No se encontró la Persona"];
    }

    return [persona, null];
  } catch (error) {
    handleError(error, "persona.service -> deletepersona");
  }
}

async function getRegistro(id, id_reg) {

  console.log(id_reg);
  
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return [null, "ID no válido. Persona no encontrada"];
  }
  if (!mongoose.Types.ObjectId.isValid(id_reg)) {
    return [null, "ID registro no válido. "];
  }

  try {
    const persona = await Persona.findById(id);

    if (!persona) {
      return [null, "No se encontró la persona"];
    }

    // Utiliza el método find en la matriz registros para buscar el registro específico
    //const registro = persona.registros.find(reg => reg._id === id_reg);
    const registro = persona.registros.id(id_reg);

    if (!registro) {
      return [null, "No se encontró el registro"];
    }

    return [registro, "Registro encontrado"];
  } catch (error) {
    handleError(error, "persona.service -> getRegistro");
  }
}


async function createPersona(rut, nombre, apellido,tipo, entra) {
  try {
    // Buscar si ya existe una persona para el rut proporcionado
    const persona = await Persona.findOne({ rut: rut });

    if (persona) {
      // Si ya existe, agregamos un nuevo registro al array de registros

      const largo = persona.registros.length;
      const ultimo_entra = largo > 0 ? persona.registros[largo - 1].entra : null;

      if (entra === ultimo_entra) {
        if (entra === "entra") {
          entra = "sale";
        }else{
          entra = "entra";
        }
      }

      persona.registros.push({
        id : largo + 1 ,
        fecha: new Date(),
        entra: entra,
      });

      await persona.save();

      return [1, `La persona ya estaba registrada y se agregó un nuevo registro. La persona estaba registrada con el NOMBRE: ${persona.nombre}, APELLIDO: ${persona.apellido} Y TIPO: ${persona.tipo}. No se modificaron estos atributos solo se creo el registro nuevo`
      ];
    } else {

      if(entra == "sale")
        return [null,"error no se puede"];
      
      // Si no existe, creamos una nueva persona
      const newpersona = new Persona({
        rut,
        nombre,
        apellido,
        tipo,
        registros: [
          {
            id : 1,
            fecha: new Date(),
            entra: entra,
          },
        ],
      });

      await newpersona.save();

      return [newpersona, "persona creada"];
    }
  } catch (error) {
    handleError(error, "persona.service -> createpersona");
    return [null, error];
  }
}


async function deletePersona(id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return [null, "id no valido"];
  }

  try {
    const persona = await Persona.findById(id);

    if (!persona) {
      return [null, "No se encontró la Persona"];
    }

    await Persona.findByIdAndDelete(id);

    return [persona, "Registro Borrado"];
  } catch (error) {
    handleError(error, "persona.service -> deletepersona");
    return [null, error];
  }
}

async function deleteRegistro(id, id_reg) {
  if (!mongoose.Types.ObjectId.isValid(id)) {
    return [null, "ID no válido. Persona no encontrada"];
  }

  if (!mongoose.Types.ObjectId.isValid(id_reg)) {
    return [null, "ID registro no válido. Persona no encontrada"];
  }

  try {
    const persona = await Persona.findById(id);
    if (!persona) {
      return [null, "No se encontró la persona"];
    }

    const registro = persona.registros.id(id_reg);

    if (registro === -1) {
      return [null, "No se encontró el registro"];
    }

    persona.registros.splice(registro, 1);
    await persona.save();

    return [registro, "Registro Borrado"];
  } catch (error) {
    handleError(error, "persona.service -> deleteRegistro");
    return [null, error];
  }
}

async function consulta(rut, tipo, entra, fechaDesde, fechaHasta) {
  try {
    const query = {};

    if (rut !== null && rut !== undefined) {
      query.rut = rut;
    }
    
    if (tipo !== null && tipo !== undefined) {
      query.tipo = tipo;
    }
    
    if (fechaHasta !== null && fechaHasta !== undefined) {
      if (fechaDesde !== null && fechaDesde !== undefined) {
        query["registros.fecha"] = { $gte: fechaDesde, $lt: fechaHasta };
      }
    }

    if (entra !== null && entra !== undefined) {
      query["registros.entra"] = entra;
    }

    const personas = await Persona.find(query);

    if (entra !== null && entra !== undefined) {
      const filteredPersonas = personas.filter(persona => 
        persona.registros.some(registro => registro.entra === entra)
      ).map(({ _id, rut, nombre, apellido, tipo, registros, __v }) => ({
        _id,
        rut,
        nombre,
        apellido,
        tipo,
        registros: registros.filter(registro => registro.entra === entra),
        __v
      }));
      return [filteredPersonas, null];
    } else {
      // En caso de que entra sea null o undefined, puedes manejarlo de la manera que prefieras
      return [personas, null];
    }
    
    
    
  } catch (error) {
    console.error("Error en la función consulta:", error);
    return [null, error];
  }
}


async function update(body) {
  if (!mongoose.Types.ObjectId.isValid(body.data.persona._id)) {
    return [null, "ID no válido. Persona no encontrada"];
  }

  
  try {
    
    const existe = await Persona.findOne({ _id: body.data.persona._id });

    if (!existe) {
      return [null, "Registro no encontrado"];
    }

    const update = await Persona.updateOne({ _id: body.data.persona._id }, {
      rut: body.data.persona.rut,
      nombre: body.data.persona.nombre,
      apellido: body.data.persona.apellido,
      tipo: body.data.persona.tipo,
      registros: body.data.persona.registros // Asumiendo que esta es una matriz de objetos
    });
    
    return [update, "Se ha modificado"];
  } catch (error) {
    handleError(error, "persona.service -> update");
    return [null, "Error al modificar la persona"];
  }
}


async function tiempoReal() {
  const today = new Date();
  today.setHours(0, 0, 0, 0);
  const tomorrow = new Date(today);
  tomorrow.setDate(today.getDate() + 1);

  try {
    const asistencia = await Asistencia.find({
      "registros.fecha": {
        $gte: today,
        $lt: tomorrow
      }
    });

    if (asistencia === null || asistencia.length === 0) {
      return [null, "No hay nadie en la municipalidad"];
    }

    const lista = [];

    for (const persona of asistencia) {
      const l = persona.registros.length;

      if (l === 0) {
        console.log(lista);
        continue;
      }

      if (persona.registros[l - 1].entra === "entra") {
        lista.push({
          rut: persona.rut,
          nombre: persona.nombre,
          apellido: persona.apellido,
          hora : persona.registros[l - 1].fecha
        });
      }
    }

    if (lista.length === 0) {
      return [null, "No hay personas que hayan entrado en la Municipalidad"];
    }

    return [lista, "Esta es la lista de las personas que están en la Municipalidad"];
  } catch (error) {
    handleError(error, "asistencia.service -> tiempoReal");
    return [null, "Error en tiempoReal"];
  }
}

module.exports = {
  getPersonas,
  getPersona,
  createPersona,
  deletePersona,
  consulta,
  getRegistro,
  deleteRegistro,
  update,
  tiempoReal,
};