const { respondSuccess, respondError } = require("../utils/resHandler");
const AsistenciaService = require("../services/asistencia.service");
const { handleError } = require("../utils/errorHandler");

const Correo  = require("../services/correo");


function validarRut(rut) {
  // Formato del RUT: xxxxxxxx-x (sin puntos)
  const rutSinPuntos = rut.replace(/\./g, '').trim();

  // Verificar formato del RUT
  const formatoValido = /^(\d{7,8})-(\d|k|K)$/.test(rutSinPuntos);
  if (!formatoValido) { 
    return false;
  }

  // Separar número y dígito verificador
  const [, numero, digitoVerificador] = rutSinPuntos.match(/^(\d+)-([\dkK])$/);

  // Calcular dígito verificador esperado
  const calcularDigitoVerificador = (numero) => {
    const serie = [2, 3, 4, 5, 6, 7, 2, 3];
    const suma = numero.split('').reverse().reduce((acc, d, i) => acc + parseInt(d) * serie[i], 0);
    const resto = suma % 11;
    const dv = 11 - resto;
    return dv === 11 ? '0' : dv === 10 ? 'k' : dv.toString();
  };

  // Verificar dígito verificador
  return digitoVerificador.toLowerCase() === calcularDigitoVerificador(numero);
}

function validarNombreoApellido(NombreoApellido) {
  // Formato del apellido: letras de la A a la Z (mayúsculas o minúsculas) con espacios opcionales
  const NombreoApellidoRegex  = /^[A-Za-z]+(?: [A-Za-z]+)?(?: [A-Za-z]+)?(?: [A-Za-z]+)?(?: [A-Za-z]+)?(?: [A-Za-z]+)?$/;

  return NombreoApellidoRegex.test(NombreoApellido);
}

exports.nuevaAsistencia = async (req, res) => {

  console.log("Correo");
  
  const rut = req.params.rut;
  const tipo = req.params.tipo;
  const nombre = req.params.nombre;
  const apellido = req.params.apellido;
  const entra = req.params.entra;

  if(!validarNombreoApellido(nombre)){
    return res.status(400).json({ error: "Nombre Formato incorrecto a-Z" });
  }

  if(!validarNombreoApellido(apellido)){
    return res.status(400).json({ error: "Apellido Formato incorrecto a-Z" });
  }

  if (!rut || !tipo) {
    return res.status(400).json({ error: "Se requieren ambos valores (rut y tipo)." });
  }

  // Verifica si el RUT es válido
  const rutValido = validarRut(rut); // Asegúrate de tener la función validarRut definida.

  if (!rutValido) {
    return res.status(400).json({ error: "El RUT no es válido." });
  }

  // Verifica si el tipo es "visitante" o "trabajador"
  if (tipo !== "visitante" && tipo !== "trabajador") {
    return res.status(400).json({ error: "El tipo debe ser 'visitante' o 'trabajador'." });
  }

  // Verifica si entra
  if (entra !== "entra" && entra !== "sale") {
    return res.status(400).json({ error: "Debe indicar si 'entra' o 'sale'." });
  }

  try {
    const [newAsistencia, respuesta] = await AsistenciaService.createPersona(rut, nombre, apellido, tipo, entra);

    if (!newAsistencia) {
      return respondError(req, res, 400, "No se pudo crear la asistencia");
    }
    
    Correo.enviarCorreo(nombre,apellido,respuesta);
    
    respondSuccess(req, res, 201, respuesta);
  } catch (error) {
    handleError(error, "Asistencia.controller -> createAsistencia");
    return respondError(req, res, 500, "No se pudo crear la asistencia");
  }
};

exports.obtenerPersonas = async (req, res) => {

  try {
    const [persona, mensaje] = await AsistenciaService.getPersonas();

    if (!persona) {
      return respondSuccess(req, res, 404, mensaje);
    } else {
      return respondSuccess(req, res, 200, { persona, mensaje });
    }
  } catch (error) {
    return respondError(req, res, 400, "Error en asistencia.controller -> obtenerPersonas");
  }
};

exports.obtenerPersona = async (req, res) => {

  const id = req.params.id;

  console.log("ID proporcionado: " + id);

  try {
    const [persona, mensaje] = await AsistenciaService.getPersona(id);

    if (!persona) {
      return respondSuccess(req, res, 404, mensaje);
    } else {
      return respondSuccess(req, res, 200, { persona, mensaje });
    }
  } catch (error) {
    return respondError(req, res, 400, "Error en asistencia.controller -> obtenerPersona");
  }
};

exports.obtenerRegistro = async (req, res) => {
  const id = req.params.id;
  const id_reg = req.params.id_reg;

  console.log("id:", id);
  console.log("id_reg:", id_reg);

  try {
    const [persona, mensaje] = await AsistenciaService.getRegistro(id,id_reg);

    if (!persona) {
      return respondSuccess(req, res, 404, mensaje);
    } else {
      return respondSuccess(req, res, 200, { persona, mensaje });
    }
  } catch (error) {
    return respondError(req, res, 400, "Error en asistencia.controller -> obtenerRegistro");
  }
};

exports.eliminarPersona = async (req, res) => {
  const id = req.params.id;

  try {
    const [persona, mensaje] = await AsistenciaService.deletePersona(id);
    console.log("Resultado del servicio: ", persona, mensaje);

    if (!persona) {
      return respondSuccess(req, res, 404, mensaje);
    } else {
      return respondSuccess(req, res, 200, { persona, mensaje });
    }
  } catch (error) {
    return respondError(req, res, 400, "Error en asistencia.controller -> eliminarAsistencia");
  }
}

exports.ElimniarRegistro = async (req, res) => {
  const id = req.params.id;
  const id_reg = req.params.id_reg;

  console.log("id:", id);
  console.log("id_reg:", id_reg);
  /*

  const validador = /^\d+$/;

  if (!id_reg.match(validador)) {
    return respondError(req, res, 400, "Ingrese un número"); 
  }
  */

  try {
    const [persona, mensaje] = await AsistenciaService.deleteRegistro(id,id_reg);

    if (!persona) {
      return respondSuccess(req, res, 404, mensaje);
    } else {
      return respondSuccess(req, res, 200, { persona, mensaje });
    }
  } catch (error) {
    return respondError(req, res, 400, "Error en asistencia.controller -> ElimniarRegistro");
  }
};

exports.consulta = async (req, res) => {
  const rut = req.query.rut ?? null;
  const tipo = req.query.tipo ?? null;
  const entra = req.query.entra ?? null;
  const fechaDesdeStr = req.query.fechaDesde?.replace(/-/g, '/') ?? null;
  const fechaHastaStr = req.query.fechaHasta?.replace(/-/g, '/') ?? null;

  let fechaDesde = null;
  let fechaHasta = null;

  if (fechaDesdeStr !== null) {
    const fechaDesdeParsed = new Date(fechaDesdeStr);
    if (!isNaN(fechaDesdeParsed)) {
      fechaDesde = fechaDesdeParsed;
    } else {
      return respondError(req, res, 400, "Rango fechaDesde no válido");
    }
  }

  if (fechaHastaStr !== null) {
    const fechaHastaParsed = new Date(fechaHastaStr);
    if (!isNaN(fechaHastaParsed)) {
      fechaHasta = fechaHastaParsed;
    } else {
      return respondError(req, res, 400, "Rango fechaHasta no válido");
    }
  }

  if (rut !== null && tipo !== null) {
    return respondError(req, res, 400, "Rangos no válidos: no puedes ingresar rut y tipo al mismo tiempo");
  }

  if (tipo !== null && tipo !== "trabajador" && tipo !== "visitante") {
    return respondError(req, res, 400, "Tipo no válido: solo se permite 'trabajador' o 'visitante'");
  }

  if (entra !== null && entra !== "entra" && entra !== "sale") {
    return respondError(req, res, 400, "Entrada no válida: solo se permite 'entra' o 'sale'");
  }

  try {
    const [asistencia, error] = await AsistenciaService.consulta(rut, tipo, entra, fechaDesde, fechaHasta);

    if (error) {
      return respondError(req, res, 500, error);
    }

    if (!asistencia || asistencia.length === 0) {
      return respondSuccess(req, res, 200, "No se encontraron registros de asistencia");
    }

    // Respuesta exitosa con contenido (código de estado 200)
    return respondSuccess(req, res, 200, asistencia);
  } catch (error) {
    console.error("Error en consulta de asistencias:", error);
    return respondError(req, res, 500, "Error en la consulta de asistencias");
  }
};

exports.modificar = async (req, res) => {
  const body = req.body;

  try {
    const [result, message] = await AsistenciaService.update(body);

    if (result) {
      return respondSuccess(req, res, 200, message);
    } else {
      return respondError(req, res, 400, message);
    }
  } catch (error) {
    console.error("Error en asistencia.controller -> modificar", error);
    return respondError(req, res, 500, "Error en asistencia.controller -> modificar");
  }
};


exports.tiempoReal = async (req, res) => {

  try {
    const [lista, respuesta] = await AsistenciaService.tiempoReal();

    if (lista == null) {
      return respondError(req, res, 200, respuesta);
    }

    return respondSuccess(req, res, 200, {"persona": lista});
  } catch (error) {
    return respondError(req, res, 500, "Error en asistencia.controller -> tiempoReal");
  }
};




exports.nuevaAsistencia2 = async (req, res) => {

  const rut = req.body.rut;
  const tipo = req.body.tipo;
  const nombre = req.body.nombre;
  const apellido = req.body.apellido;
  const entra = req.body.entra;

  if(!validarNombreoApellido(nombre)){
    return res.status(400).json({ error: "Nombre Formato incorrecto a-Z" });
  }

  if(!validarNombreoApellido(apellido)){
    return res.status(400).json({ error: "Apellido Formato incorrecto a-Z" });
  }

  if (!rut || !tipo) {
    return res.status(400).json({ error: "Se requieren ambos valores (rut y tipo)." });
  }

  // Verifica si el RUT es válido
  const rutValido = validarRut(rut); // Asegúrate de tener la función validarRut definida.

  if (!rutValido) {
    return res.status(400).json({ error: "El RUT no es válido." });
  }

  // Verifica si el tipo es "visitante" o "trabajador"
  if (tipo !== "visitante" && tipo !== "trabajador") {
    return res.status(400).json({ error: "El tipo debe ser 'visitante' o 'trabajador'." });
  }

  // Verifica si entra
  if (entra !== "entra" && entra !== "sale") {
    return res.status(400).json({ error: "Debe indicar si 'entra' o 'sale'." });
  }

  try {
    const [newAsistencia, respuesta] = await AsistenciaService.createPersona(rut, nombre, apellido, tipo, entra);

    if (!newAsistencia) {
      return respondError(req, res, 400, "No se pudo crear la asistencia");
    }
    
    Correo.enviarCorreo(nombre,apellido,respuesta);
    
    respondSuccess(req, res, 201, respuesta);
  } catch (error) {
    handleError(error, "Asistencia.controller -> createAsistencia");
    return respondError(req, res, 500, "No se pudo crear la asistencia");
  }
};