Requisitos
1 Registrar entrada y salida
El guardia registrará en el sistema las personas que ingresen la hora y fecha lo agregara el sistema. No podrá ingresar la asistencia fuera del horario de la municipalidad y tampoco un sábado y domingo. y además indicará si entro o salio
2 Mostrar listas para auditoría
El administrador podrá generar consultas, por ejemplo: todas las personas registradas en el sistema o una específica, o solo lista de trabajadores o visitantes, o solo lista de los que entraron o salieron, o la lista en un rango específico. También puede ser una combinación de estos parámetros como los trabajadores que entraron el día de hoy.
3 Mostrar notificaciones
El sistema mostrará notificaciones  por correo de las personas que van entrando y saliendo.
4 Actualizar datos y eliminar
Se podrán actualizar los datos de personas registradas o sus registros de asistencia la municipalidad. También se podrán borrar la  persona o un registro específico.
5 Ver personas que están en el edificio
El administrador y el guardia podrá generar una lista con todas las personas que están en el edificio.

