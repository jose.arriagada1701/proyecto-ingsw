import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import dotenv from 'dotenv';

dotenv.config();

const port = process.env.PORT || 2222;

console.log(`Configurando el puerto: ${port}`);

export default defineConfig({
  plugins: [react()],
  server: {
    port: parseInt(port),
	watch: {
      usePolling: true,
      interval: 1000,
    },  
},
});

