import { useNavigate } from 'react-router-dom';
import { logout } from '../services/auth.service';


const NavBar = () => {
  const navigate = useNavigate();
  const handleLogout = () => {
    logout();
    navigate('/auth');
  };
  return (
    <nav className="nav nav-pills nav-justified">
      <li>
      <button
          className="nav-link btn btn-primary btn-principal" style={{ color: '#F2EDD5' }}// Añade la clase btn-principal
          onClick={() => navigate('/')}
        >
          Principal
        </button>
      </li>

      <li>
        <button className="nav-link btn btn-info" style={{ color: '#F2EDD5' }} onClick={() => navigate('/crear')}>
          Crear Asistencia
        </button>
      </li>
      <li>
        <button className="nav-link btn btn-warning" style={{ color: '#F2EDD5' }} onClick={() => navigate('/consulta')}>
          Consulta
        </button>
      </li>
      
      <li>
        <button className="nav-link btn btn-secondary" style={{ color: '#F2EDD5' }} onClick={() => navigate('/envivo')}>
          En Vivo
        </button>
      </li>

      <li>
        <button className="nav-link btn btn-danger" style={{ color: '#F2EDD5' }} onClick={() => navigate('/admin')}>
          Administrador
        </button>
      </li>

      <li>
        <button className="btn btn-danger" style={{ color: 'white' }} onClick={handleLogout}>
          Cerrar sesión
        </button>
      </li>
    </nav>
  );
};

export default NavBar;

/*
<li>
        <button className="nav-link btn btn-success" style={{ color: '#F2EDD5' }} onClick={() => navigate('/historial')}>
          Historial
        </button>
      </li>
*/