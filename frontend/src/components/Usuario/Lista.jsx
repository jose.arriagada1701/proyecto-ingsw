import React, { useEffect, useState } from "react";
import { getUsers, deleteUser } from "../../services/user";

function Lista() {
  const [usuarioData, setUsuarioData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await getUsers();
        console.log("Solicitud de asistencia realizada con éxito:", response);
        setUsuarioData(response.data);
      } catch (error) {
        console.error("Error al realizar la solicitud de asistencia:", error);
      }
    };

    fetchData();
  }, []);

  const handleDeleteUser = async (id) => {
    try {
      const response = await deleteUser(id);
      console.log("Usuario eliminado con éxito:", response);
      // Actualiza la lista de usuarios después de la eliminación
      const updatedUsers = usuarioData.filter((user) => user._id !== id);
      setUsuarioData(updatedUsers);
    } catch (error) {
      console.error("Error al eliminar usuario:", error);
    }
  };

  const filteredData = usuarioData.filter((user) => {
    // Verifica si el usuario tiene roles y ninguno de ellos es 'admin'
    return user.roles && !user.roles.some((role) => role.name === "admin");
  });

  return (
    <div className="container mt-4">
      <h1>Lista de Usuarios</h1>
      <ul className="list-group">
        {filteredData.map((usuario) => (
          <li key={usuario._id} className="list-group-item d-flex justify-content-between align-items-center">
            <div>
              <strong>Usuario:</strong> {usuario.username} | <strong>Email:</strong> {usuario.email}
            </div>
            <button
              className="btn btn-danger"
              onClick={() => handleDeleteUser(usuario._id)}
            >
              Eliminar
            </button>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default Lista;
