import React, { useEffect, useState } from "react";
import { getUsers, createUser } from "../../services/user";

function Lista() {
  const [usuarioData, setUsuarioData] = useState([]);
  const [newUserData, setNewUserData] = useState({
    username: "",
    email: "",
    password: "",
    roles: ["user"],
  });

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await getUsers();
        console.log("Solicitud de asistencia realizada con éxito:", response);
        setUsuarioData(response.data);
      } catch (error) {
        console.error("Error al realizar la solicitud de asistencia:", error);
      }
    };

    fetchData();
  }, []);

  const handleCreateUser = async () => {
    try {
      const response = await createUser(newUserData);
      console.log("Usuario creado con éxito:", response);

      // Actualiza la lista de usuarios después de la creación
      const updatedUsers = [...usuarioData, response];
      setUsuarioData(updatedUsers);
      window.location.reload();
      // Reinicia los datos del nuevo usuario
      setNewUserData({
        username: "",
        email: "",
        password: "",
        roles: ["user"],
      });
    } catch (error) {
      console.error("Error al crear usuario:", error);
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setNewUserData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  return (
    <div className="container mt-4">
      

      <div className="mt-4">
        <h2>Crear Nuevo Usuario</h2>
        <form>
          <div className="mb-3">
            <label htmlFor="username" className="form-label">
              Usuario
            </label>
            <input
              type="text"
              className="form-control"
              id="username"
              name="username"
              value={newUserData.username}
              onChange={handleChange}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="email" className="form-label">
              Email
            </label>
            <input
              type="email"
              className="form-control"
              id="email"
              name="email"
              value={newUserData.email}
              onChange={handleChange}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="password" className="form-label">
              Contraseña
            </label>
            <input
              type="password"
              className="form-control"
              id="password"
              name="password"
              value={newUserData.password}
              onChange={handleChange}
            />
          </div>
          <button type="button" className="btn btn-primary" onClick={handleCreateUser}>
            Crear Usuario
          </button>
        </form>
      </div>
      <br></br>
      <br></br>
      <br></br>
    </div>
  );
}

export default Lista;
