import { useNavigate } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { login } from '../services/auth.service';

function LoginForm() {
  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    login(data).then(() => {
      navigate('/');
    });
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div className="mb-3">
        <label htmlFor="email" className="form-label">Email</label>
        <input
          name="email"
          type="email"
          className={`form-control ${errors.email ? 'is-invalid' : ''}`}
          style={{ backgroundColor: '#407364', color: '#F2EDD5' }}
          {...register('email', { required: true })}
        />
        {errors.email && <div className="invalid-feedback">Este campo es obligatorio</div>}
      </div>

      <div className="mb-3">
        <label htmlFor="password" className="form-label">Contraseña</label>
        <input
          type="password"
          name="password"
          className={`form-control ${errors.password ? 'is-invalid' : ''}`}
          style={{ backgroundColor: '#768C54', color: '#F2EDD5' }}
          {...register('password', { required: true })}
        />
        {errors.password && <div className="invalid-feedback">Este campo es obligatorio</div>}
      </div>

      <button type="submit" className="btn btn-light" style={{ backgroundColor: '#0B2626', color: '#F2EDD5' }}>
        Enviar
      </button>
    </form>
  );
};



export default LoginForm;
