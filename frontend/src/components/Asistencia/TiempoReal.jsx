import React, { useEffect, useState } from 'react';
import { tiempoReal } from '../../services/asistencia';
import Persona from './Persona';
import moment from 'moment';
import 'moment/locale/es';


moment.lang('es', {
  months: 'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split('_'),
  monthsShort: 'Enero._Feb._Mar_Abr._May_Jun_Jul._Ago_Sept._Oct._Nov._Dec.'.split('_'),
  weekdays: 'Domingo_Lunes_Martes_Miercoles_Jueves_Viernes_Sabado'.split('_'),
  weekdaysShort: 'Dom._Lun._Mar._Mier._Jue._Vier._Sab.'.split('_'),
  weekdaysMin: 'Do_Lu_Ma_Mi_Ju_Vi_Sa'.split('_')
}
);

function TiempoReal() {
  const [asistenciaData, setAsistenciaData] = useState([]);
  const [message, setMessage] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await tiempoReal();
        setAsistenciaData(response); // Modificado para acceder a la propiedad 'persona' de la respuesta
      } catch (error) {
        console.error('Error al realizar la solicitud de asistencia:', error);
        setMessage('Error al cargar la asistencia'); // Mensaje de error en caso de fallo
      }
    };

    fetchData();
  }, []);

  // Verifica si asistenciaData.data.persona es un array antes de intentar mapear
  const personas = Array.isArray(asistenciaData.data?.persona) ? asistenciaData.data.persona : [];

  console.log();
  return (
    <>
      {asistenciaData.message && <h3>{asistenciaData.message}</h3>}
      <table className="table table-bordered">
        <thead>
          <tr>
            <th>RUT</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Hora de Entrada</th>
          </tr>
        </thead>
        <tbody>
          {personas.map((persona, index) => (
            <tr key={index}>
              <td>{persona.rut}</td>
              <td>{persona.nombre}</td>
              <td>{persona.apellido}</td>
              <td>{moment(persona.hora).format('HH:mm:ss')}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
  
}

export default TiempoReal;
