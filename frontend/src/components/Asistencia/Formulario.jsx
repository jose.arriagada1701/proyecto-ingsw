import React from "react";
import { useForm } from "react-hook-form";
import { enviarAsistencia } from "../../services/asistencia";

function validarRut(rut) {
  const rutSinPuntos = rut.replace(/\./g, '').trim();
  const formatoValido = /^(\d{7,8})-(\d|k|K)$/.test(rutSinPuntos);

  if (!formatoValido) {
    return false;
  }

  const [, numero, digitoVerificador] = rutSinPuntos.match(/^(\d+)-([\dkK])$/);

  const calcularDigitoVerificador = (numero) => {
    const serie = [2, 3, 4, 5, 6, 7, 2, 3];
    const suma = numero.split('').reverse().reduce((acc, d, i) => acc + parseInt(d) * serie[i], 0);
    const resto = suma % 11;
    const dv = 11 - resto;
    return dv === 11 ? '0' : dv === 10 ? 'k' : dv.toString();
  };

  return digitoVerificador.toLowerCase() === calcularDigitoVerificador(numero);
}

function validarNombreoApellido(NombreoApellido) {
  const NombreoApellidoRegex = /^[A-Za-z]+(?: [A-Za-z]+)?(?: [A-Za-z]+)?(?: [A-Za-z]+)?(?: [A-Za-z]+)?(?: [A-Za-z]+)?$/;
  return NombreoApellidoRegex.test(NombreoApellido);
}

const Formulario = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = async (data) => {
    try {
      const { rut, nombre, apellido, tipo, entra } = data;

      if (!validarRut(rut)) {
        alert("RUT no válido");
        return;
      }

      if (!validarNombreoApellido(nombre) || !validarNombreoApellido(apellido)) {
        alert("Nombre o Apellido no válido");
        return;
      }

      const response = await enviarAsistencia(rut, nombre, apellido, tipo, entra);
      console.log(response);
      alert("Asistencia enviada con éxito");
    } catch (error) {
      console.error('Error al enviar asistencia:', error.message);
    }
  };

  return (
    <>
      <h2>Crear Asistencia</h2>
      <form className="container mt-4" onSubmit={handleSubmit(onSubmit)}>
        <div className="mb-3">
          <label htmlFor="rut" className="form-label">
            Rut
          </label>
          <input
            placeholder="11111111-1"
            type="text"
            className={`form-control ${errors.rut ? 'is-invalid' : ''}`}
            id="rut"
            {...register('rut', {
              required: 'Este campo es requerido',
              pattern: {
                value: /^[0-9]+-[0-9kK]{1}$/i,
                message: 'Ingresa un Rut válido (ej. 12345678-9)',
              },
            })}
          />
          {errors.rut && (
            <div className="invalid-feedback">{errors.rut.message}</div>
          )}
        </div>
        <div className="mb-3">
          <label htmlFor="nombre" className="form-label">
            Nombre
          </label>
          <input
            placeholder="Juan Pedro"
            type="text"
            className={`form-control ${errors.nombre ? 'is-invalid' : ''}`}
            id="nombre"
            {...register('nombre', {
              required: 'Este campo es requerido',
              pattern: {
                value: /^[A-Za-z]+(?: [A-Za-z]+)?(?: [A-Za-z]+)?(?: [A-Za-z]+)?(?: [A-Za-z]+)?(?: [A-Za-z]+)?$/,
                message: 'Ingresa solo letras en el nombre',
              },
            })}
          />
          {errors.nombre && (
            <div className="invalid-feedback">{errors.nombre.message}</div>
          )}
        </div>
        <div className="mb-3">
          <label htmlFor="apellido" className="form-label">
            Apellido
          </label>
          <input
            placeholder="Rivera Rojo"
            type="text"
            className={`form-control ${errors.apellido ? 'is-invalid' : ''}`}
            id="apellido"
            {...register('apellido', {
              required: 'Este campo es requerido',
              pattern: {
                value: /^[A-Za-z]+(?: [A-Za-z]+)?(?: [A-Za-z]+)?(?: [A-Za-z]+)?(?: [A-Za-z]+)?(?: [A-Za-z]+)?$/,
                message: 'Ingresa solo letras en el apellido',
              },
            })}
          />
          {errors.apellido && (
            <div className="invalid-feedback">{errors.apellido.message}</div>
          )}
        </div>
        <div className="mb-3">
          <label htmlFor="tipo" className="form-label">
            Tipo de Entrante: (Visitante/Trabajador)
          </label>
          <select className="form-select" id="tipo" {...register('tipo')}>
            <option value="visitante">Visitante</option>
            <option value="trabajador">Trabajador</option>
          </select>
        </div>
        <p>*Tipo de personas que entran a la municipalidad (Visitante o Trabajador)</p>
        <div className="mb-3">
          <label htmlFor="entra" className="form-label">
            Estado de Ingreso (Entra/Sale)
          </label>
          <select className="form-select" id="entra" {...register('entra')}>
            <option value="entra">Entra</option>
            <option value="sale">Sale</option>
          </select>
        </div>
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
      <br />
      <br />
      <br />
      <br />
    </>
  );
};

export default Formulario;
