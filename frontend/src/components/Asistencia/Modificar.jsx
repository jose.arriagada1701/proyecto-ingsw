import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { obtenerPersona, eliminarPersona, eliminarRegistro, actualizar} from '../../services/asistencia';
import moment from 'moment';


function Modificar({ id }) {
  const { register, handleSubmit, setValue, formState: { errors } } = useForm();
  const [personaData, setPersonaData] = useState({});
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await obtenerPersona(id);
        
        if (!response || !response.data || !response.data.persona) {
          setError('Persona no encontrada');
        } else {
          setPersonaData(response.data.persona);
          // Set initial form values
          setValue('nombre', response.data.persona.nombre);
          setValue('apellido', response.data.persona.apellido);
          setValue('tipo', response.data.persona.tipo);
          setValue('entra', ''); // Assuming entra is editable, you can set an initial value or leave it empty
        }
      } catch (error) {
        console.error('Error al realizar la solicitud de asistencia:', error);
        setError('Persona no encontrada');
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [id, setValue]);

  const handleEliminarPersona = async () => {
    try {
      const respuesta= await eliminarPersona(id);
      alert(respuesta);
      window.location.reload();
    } catch (error) {
      console.error('Error al eliminar la persona:', error);
    }
  };

  const handleEliminarRegistro = async (id_persona, id_registro) => {
    try {
      const respuesta = await eliminarRegistro(id_persona, id_registro);
      console.log(respuesta)
      window.location.reload();
    } catch (error) {
      console.error('Error al eliminar el registro:', error);
    }
  };
 
  const onSubmit = async (data, dataPersona) => {

    const nuevaPersonaData = {
      _id: dataPersona._id,
      nombre: data.nombre,
      rut: data.rut,
      apellido: data.apellido,
      tipo: data.tipo,
    };
  
    const registros = {
      registros: Object.entries(data).map(([id, tipo]) => ({ _id: id, entra: tipo })),
    };
  
    
    const resultadoFusionado = {
      registros: dataPersona.registros.map((obj1, index) => ({
        entra: obj1.entra,
        fecha: moment(obj1.fecha).locale('es', { useStrict: true }).format("YYYY-MM-DDTHH:mm:ss.SSSZ")
      })),
    };
    
    const nuevaPersonaDataFusionada = {
      data: {
        persona: {
          ...nuevaPersonaData,
          //REGISTROS
            ...resultadoFusionado
          
        }
      },
    };

    console.log(nuevaPersonaDataFusionada);
    
  
    try {
      const respuesta =await actualizar(nuevaPersonaDataFusionada);
      console.log(respuesta);
      alert(respuesta.data);
      window.location.reload();
    } catch (error) {
      console.error('Error al Actualizar el registro:', error);
    }
  

  };
  
  

  

  if (loading) {
    return <h2>Cargando...</h2>;
  }

  if (error) {
    return <h2>{error}</h2>;
  }

  // ... (importaciones y funciones anteriores)
return (
  <>
    {personaData && (
      <form className="container mt-4" onSubmit={handleSubmit((data) => onSubmit(data, personaData))}>
        <h2>Persona:</h2>
        <button className="btn btn-danger " style= {{color:'white'}} onClick={handleEliminarPersona}>
          Eliminar Persona
        </button>
        

        <div className="mb-3">
          <label htmlFor="rut" className="form-label">Rut</label>
          <input
            type="text"
            name="rut"
            id="rut"
            className={`form-control ${errors.rut ? 'is-invalid' : ''}`}
            {...register('rut', {
              required: "Este campo es requerido",
              pattern: {
                value: /^[0-9]+-[0-9kK]{1}$/i,
                message: "Ingresa un Rut válido (ej. 12345678-9)",
              },
            })}
            defaultValue={personaData.rut}
          />
          {errors.rut && <p className="invalid-feedback">{errors.rut.message}</p>}
        </div>

        

        <div className="mb-3">
          <label htmlFor="nombre" className="form-label">Nombre</label>
          <input type="text" {...register('nombre')} defaultValue={personaData.nombre} className="form-control" />
        </div>

        <div className="mb-3">
          <label htmlFor="apellido" className="form-label">Apellido</label>
          <input type="text" {...register('apellido')} defaultValue={personaData.apellido} className="form-control" />
        </div>

        <div className="mb-3">
          <label htmlFor="tipo" className="form-label">Tipo (visitante/trabajador)</label>
          <select name="tipo" id="tipo" {...register('tipo')} defaultValue={personaData.tipo} className="form-select">
            <option value="visitante">Visitante</option>
            <option value="trabajador">Trabajador</option>
          </select>
        </div>

        {personaData.registros.length > 0 && (
          <div>
            <table className="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Registro ID</th>
                  <th>Fecha</th>
                  <th>Estado</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>
                {personaData.registros.map((registro, regIndex) => (
                  <tr key={regIndex}>
                    <td>{regIndex + 1}</td>
                    <td>{moment(registro.fecha).format('LL')}</td>
                    <td>
                      <select name={`${registro._id}`} {...register(`${registro._id}`)} defaultValue={registro.entra} className="form-select">
                        <option value="entra">entra</option>
                        <option value="sale">sale</option>
                      </select>
                    </td>
                    <td>
                      <button onClick={() => handleEliminarRegistro(personaData._id, registro._id)} className="btn btn-danger">
                        Eliminar
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        )}

        <button type="submit" className="btn btn-primary">Guardar Cambios</button>
        <br/>
        <br/>
        <br/>
        <br/>  
      </form>
      
    )}
  </>
);
                }

export default Modificar;
