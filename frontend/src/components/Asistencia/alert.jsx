// Alert.js
import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'; // Importa Bootstrap

const Alert = ({ message, onClose }) => {
  return (
    <div className="alert alert-primary custom-alert" role="alert">
      <p>{message}</p>
      <button type="button" className="close" aria-label="Close" onClick={onClose}>
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  );
};

export default Alert;
