import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import 'moment/locale/es';


moment.lang('es', {
  months: 'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split('_'),
  monthsShort: 'Enero._Feb._Mar_Abr._May_Jun_Jul._Ago_Sept._Oct._Nov._Dec.'.split('_'),
  weekdays: 'Domingo_Lunes_Martes_Miercoles_Jueves_Viernes_Sabado'.split('_'),
  weekdaysShort: 'Dom._Lun._Mar._Mier._Jue._Vier._Sab.'.split('_'),
  weekdaysMin: 'Do_Lu_Ma_Mi_Ju_Vi_Sa'.split('_')
}
);


const Persona = ({ Persona }) => {
  return (
    <>
      {Array.isArray(Persona) &&
        Persona.map((persona, index) => (
          <div key={index} className="persona card mb-3">
            <div className="card-body">
              <h2 className="card-title" style={{color: '#2C6C73'}}>Persona</h2>
              <ul className="list-unstyled">
                <li>RUT: {persona.rut}</li>
                <li>Nombre: {persona.nombre}</li>
                <li>Tipo: {persona.tipo}</li>
                <li>Apellido: {persona.apellido}</li>
                {persona.registros.length > 0 && (
                  <li>
                    <table className="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th>Número</th>
                          <th>Fecha</th>
                          <th>Entrada</th>
                        </tr>
                      </thead>
                      <tbody>
                        {persona.registros.map((registro, regIndex) => (
                          <tr key={regIndex}>
                            <td>{regIndex + 1}</td>
                            <td>{moment(registro.fecha).format('LLL')}</td>
                            <td>{registro.entra}</td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                  </li>
                )}
              </ul>
              <Link to={`/editar/${persona._id}`}>
                <button className="btn btn-primary">Modificar</button>
              </Link>
            </div>
          </div>
        ))}
    </>
  );
};

export default Persona;




