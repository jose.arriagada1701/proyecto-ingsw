import React, { useEffect, useState } from 'react';
import { obtenerAsistencia, tiempoReal } from '../../services/asistencia';
import Persona from './Persona';

function Historial() {
  const [asistenciaData, setAsistenciaData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await obtenerAsistencia();
        console.log('Solicitud de asistencia realizada con éxito:', response);
        setAsistenciaData(response.data.persona); // Modificado para acceder a response.data.persona
      } catch (error) {
        console.error('Error al realizar la solicitud de asistencia:', error);
      }
    };

    fetchData();
  }, []);

  return (
    <>
      <Persona Persona={asistenciaData}/>
    </>
  );
}

export default Historial;