import React, { useState } from "react";
import { useForm, Controller } from "react-hook-form";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { consulta } from "../../services/asistencia";
import Persona from "./Persona";

const ConsultaForm = () => {
  const { register, handleSubmit, control } = useForm();
  const [resultadosConsulta, setResultadosConsulta] = useState([]);

  const onSubmit = async (data) => {
    try {
      const rut = /^[0-9]+-[0-9kK]{1}$/i.test(data.rut) ? data.rut : null;
      if(rut==null)
        alert("Rut invalido, no se considerará")
      const tipo = data.tipo === "todos" ? null : data.tipo;
      const entraSale = data.entraSale === "todos" ? null : data.entraSale;

      const result = await consulta(rut, tipo, entraSale, data.fechaInicio, data.fechaFin);

      console.log(result.data);
      setResultadosConsulta(result.data);
    } catch (error) {
      console.error('Error al realizar la consulta:', error.message);
    }
  };

  //const resultadosConsultaJson = JSON.stringify(resultadosConsulta);
  //console.log(resultadosConsultaJson);

  return (
    <div className="container mt-4">
      <form onSubmit={handleSubmit(onSubmit)} className="mb-4">
        <div className="mb-3">
          <label htmlFor="rut" className="form-label">Rut:</label>
          <input {...register('rut')} type="text" className="form-control" placeholder="20194737-7 (Opcional)" />
        </div>
        <div className="mb-3">
          <label htmlFor="tipo" className="form-label">Tipo:</label>
          <select {...register('tipo')} className="form-select">
            <option value="todos">Todos</option>
            <option value="visitante">Visitante</option>
            <option value="trabajador">Trabajador</option>
          </select>
        </div>
        <p>*Se puede elegir listar a todas las personas sean vistantes o trabajadores o a alguna especifica</p>
        <div className="mb-3">
          <label htmlFor="entraSale" className="form-label">Entrada/Salida:</label>
          <select {...register('entraSale')} className="form-select">
            <option value="todos">Todos</option>
            <option value="entra">Entra</option>
            <option value="sale">Sale</option>
          </select>
        </div>
        <p>*Se puede elegir listar a todas las personas que entraron o que salieron o alguna especifica</p>
        <div className="mb-3">
          <label htmlFor="fechaInicio" className="form-label">Rango de Fechas:</label>
          <p>Se puede elegir un rango de fechas donde se hace la consula (Opcional)</p>
          <div className="mb-3">
            <label htmlFor="fechaInicio" className="form-label">Fecha de inicio:</label>
            <br/>
            <Controller
              control={control}
              name="fechaInicio"
              render={({ field }) => (
                <DatePicker
                  selected={field.value}
                  onChange={(date) => field.onChange(date)}
                  dateFormat="yyyy-MM-dd"
                  placeholderText="Fecha de inicio"
                  className="form-control"
                />
              )}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="fechaFin" className="form-label  mb-3">Fecha de fin: </label>
            <br/>
            <Controller
              control={control}
              name="fechaFin"
              render={({ field }) => (
                <DatePicker
                  selected={field.value}
                  onChange={(date) => field.onChange(date)}
                  dateFormat="yyyy-MM-dd"
                  placeholderText="Fecha de fin"
                  className="form-control"
                />
              )}
            />
          </div>
        </div>
        <button type="submit" className="btn btn-primary">Buscar</button>
      </form>
  
      <div>
        <h2>
          Resultados Consulta 
        </h2> 
        <Persona Persona={resultadosConsulta}/>
        
          
        
      </div>
    </div>
  );
  
}

export default ConsultaForm;
