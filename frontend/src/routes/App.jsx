import React from 'react';

function App() {
  return (
    
    <div className="container mt-4 mb-5" >
      <br></br>
    <br></br>
    <br></br>
    <h1 className="text-center display-4">Presentación de Proyecto</h1>
    <br></br>
    <br></br>

      <p className="text-justify">
        El proyecto está enmarcado en la transformación digital del Estado y enfocado en los
        Municipios, en concreto se encargó a este equipo el control, flujo y personal: esto consiste en
        saber quién entra y sale de un municipio y, luego poder consultar de manera rápida y confiable
        quién ha entrado y salido en el pasado.
      </p>
      <p className="text-justify">
        Para la realización de este proyecto de avance se han realizado entrevistas a usuarios de
        municipios y a un cliente ficticio que hace de solicitante de este software. Es un proyecto
        ficticio para el ramo de Ingeniería de Software que pretende desarrollar habilidades de
        desarrollo de un producto de Software. Se desarrollará en el Stack MERN que es una fusión de
        tecnologías para desarrollo en esta área.
      </p>
      <br></br>
    
      <p className=" mt-3">
        - Documentación del Proyecto: {' '}
        <a
          href="https://gitlab.com/jose.arriagada1701/proyecto-ingsw/-/raw/main/informe.pdf?ref_type=heads&inline=false"
          target="_blank"
          rel="noopener noreferrer"
          className="btn btn-primary"
        >
          Documentación Enlace
        </a>{' '}
        <br></br>
        - Repositorio del Proyecto: {' '}
        <a
          href="https://gitlab.com/jose.arriagada1701/proyecto-ingsw"
          target="_blank"
          rel="noopener noreferrer"
          className="btn btn-primary"
        >
          Repositorio Enlace
        </a>
      </p>
      
      <br></br>
    <br></br>
    <br></br>
    
    </div>
  );
}

export default App;


