import React from 'react';
import Historial from '../components/Asistencia/Historial';

function Asistencia() {
  return (
    <div>
      <h2>Historial de Asistencia</h2>
      <Historial/>
    </div>
  );
}

export default Asistencia;
