import React from 'react';
import ConsultaForm from '../components/Asistencia/ConsultaForm';
import Historial from '../components/Asistencia/Historial';

function Consulta() {

  return (

    <>
    <h2>Hacer un Consulta</h2>
		<ConsultaForm />
    <div>
      <h2>Historial</h2>
      <Historial/>
    </div>

    </>

  );
}

export default Consulta;