import { Outlet } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { logout } from '../services/auth.service';
import { AuthProvider, useAuth } from '../context/AuthContext';
import NavBar from '../components/NavBar';

import logo from '../img/logo.png'
import '../index.css'
function Root() {
  return (
    <AuthProvider>
      <PageRoot />
    </AuthProvider>
  );
}

function PageRoot() {
  const navigate = useNavigate();

  const handleLogout = () => {
    logout();
    navigate('/auth');
  };

  const { user } = useAuth();

  return (
    <div>

      
      <div className="header">

        <div className="app-container ">
          
          <img src={logo} alt="Logo de la aplicación" className="logo" />
          <h1 className="main-heading titulo" style={{ color: '#F2EDD5' }}>

          Sistema Asistencia Municipio </h1>

          </div>
            <p style={{ color: '#F2EDD5' }}>Estás logeado como: {user.email}</p>
            <NavBar />
        </div>

        <Outlet />
        
        <footer > © Sistema de Asistencia Municipio, derechos Reservados</footer>
    </div>

    
  );
}

export default Root;

