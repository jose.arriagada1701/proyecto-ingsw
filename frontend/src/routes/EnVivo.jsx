// EnVivo.js
import React from 'react';
import TiempoReal from '../components/Asistencia/TiempoReal';

function EnVivo() {
  return (
    <>
      <div>
        <h2>En el Municipio</h2>
        <TiempoReal />
      </div>
    </>
  );
}

export default EnVivo;
