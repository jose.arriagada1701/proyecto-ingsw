import React from 'react';
import Modificar from '../components/Asistencia/Modificar';
import { useParams } from 'react-router-dom';


function Editar() {
  const params = useParams();
  console.log(params.id);

  return (
    <div>
      <h2>Editar</h2>
	  <Modificar id={params.id}/>
     
    </div>
  );
}

export default Editar;