import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

import React from 'react';
import { createRoot } from 'react-dom/client'; // Update import statement
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import App from './routes/App.jsx';
import Root from './routes/Root.jsx';
import ErrorPage from './routes/ErrorPage.jsx';
import Login from './routes/Login.jsx';
import Asistencia from './routes/Asistencia.jsx';
import EnVivo from './routes/EnVivo.jsx';
import Editar from './routes/Editar.jsx';
import Crear from './routes/Crear.jsx';
import Consulta from './routes/Consulta.jsx';
import CrearUsuario from './routes/CrearUsuario.jsx';

// Use createRoot to render your application
const root = createRoot(document.getElementById('root'));

root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

const router = createBrowserRouter([
  {
    path: '/',
    element: <Root />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: '/',
        element: <App />,
      },
      {
        path: '/historial',
        element: <Asistencia />,
      },
      {
        path: '/envivo',
        element: <EnVivo />,
      },
      {
        path: '/editar/:id',
        element: <Editar />,
      },
      {
        path: '/crear',
        element: <Crear />,
      },
      {
        path: '/consulta',
        element: <Consulta />,
      },
      {
        path: '/admin',
        element: <CrearUsuario />,
      },
    ],
  },
  {
    path: '/auth',
    element: <Login />,
  },
]);

root.render(
  <RouterProvider router={router} />
);

