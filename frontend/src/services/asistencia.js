import axios from './root.service';


const API_URL = import.meta.env.VITE_BASE_URL || 'http://localhost:5000/api';

// Obtener el historial de la base de datos
export const obtenerAsistencia = async () => {
  try {
    const userId = localStorage.getItem('user');

    if (!userId) {
      console.error('User data not found in localStorage');
      return;
    }

    const userObject = JSON.parse(userId);

    // Use the _id from the first role as the accessToken
    const accessToken = userObject.roles.length > 0 ? userObject.roles[0]._id : null;

    if (!accessToken) {
      console.error('Access token not found in user roles');
      return;
    }


    try {
      const ruta = `${API_URL}/asistencia`;

      const response = await axios.get(ruta, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      });

      console.log(response);

      if (response.status === 200) {
        const asistenciaData = response.data; 
        return asistenciaData; 
      } else {
        console.error('Error fetching asistencia data:', response.statusText);
      }
    } catch (error) {
      console.error('Error fetching asistencia:', error.message);
    }
  } catch (error) {
    console.error('Error parsing user data:', error.message);
  }
};

// Quien esta en la municipalidad
export const tiempoReal = async () => {
  try {
    const userId = localStorage.getItem('user');

    if (!userId) {
      console.error('User data not found in localStorage');
      return;
    }

    const userObject = JSON.parse(userId);

    // Use the _id from the first role as the accessToken
    const accessToken = userObject.roles.length > 0 ? userObject.roles[0]._id : null;

    if (!accessToken) {
      console.error('Access token not found in user roles');
      return;
    }


    try {
      const ruta = `${API_URL}/asistencia/tiempoReal`;

      const response = await axios.get(ruta, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      });

      //console.log(response);

      if (response.status === 200) {
        const asistenciaData = response.data; // Cambio aquí: extrae la propiedad .data del response
        // Puedes manejar asistenciaData según sea necesario
        // console.log('Asistencia Data:', asistenciaData);
        return asistenciaData; // Cambio aquí: devuelve asistenciaData en lugar de response.data
      } else {
        console.error('Error fetching asistencia data:', response.statusText);
      }
    } catch (error) {
      console.error('Error fetching asistencia:', error.message);
    }
  } catch (error) {
    console.error('Error parsing user data:', error.message);
  }
};

// Quien esta en la municipalidad
export const obtenerPersona = async (id) => {
  try {
    const userId = localStorage.getItem('user');

    if (!userId) {
      console.error('User data not found in localStorage');
      return;
    }

    const userObject = JSON.parse(userId);

    // Use the _id from the first role as the accessToken
    const accessToken = userObject.roles.length > 0 ? userObject.roles[0]._id : null;

    if (!accessToken) {
      console.error('Access token not found in user roles');
      return;
    }

    console.log(accessToken);

    try {
      const ruta = `${API_URL}/asistencia/${id}`;

      const response = await axios.get(ruta, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      });

      //console.log(response);

      if (response.status === 200) {
        const asistenciaData = response.data; // Cambio aquí: extrae la propiedad .data del response
        // Puedes manejar asistenciaData según sea necesario
        // console.log('Asistencia Data:', asistenciaData);
        return asistenciaData; // Cambio aquí: devuelve asistenciaData en lugar de response.data
      } else {
        console.error('Error fetching asistencia data:', response.statusText);
      }
    } catch (error) {
      console.error('Error fetching asistencia:', error.message);
    }
  } catch (error) {
    console.error('Error parsing user data:', error.message);
  }
};


// Función para enviar datos de asistencia mediante una solicitud POST
export const enviarAsistencia = async (rut, nombre, apellido, tipo, entra) => {
  try {
    const userId = localStorage.getItem('user');

    if (!userId) {
      console.error('User data not found in localStorage');
      return;
    }

    const userObject = JSON.parse(userId);

    // Utiliza el _id del primer rol como accessToken
    const accessToken = userObject.roles.length > 0 ? userObject.roles[0]._id : null;

    if (!accessToken) {
      console.error('Access token not found in user roles');
      return;
    }

    try {
      const ruta = `${API_URL}/asistencia/`;

      const datos = {
        rut: rut,
        nombre: nombre,
        apellido: apellido,
        tipo: tipo,
        entra: entra
      };
      
      const response = await axios.post(ruta, datos, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
          'Content-Type': 'application/json',
        },
      });

      if (response.status === 201) {
        console.log('Asistencia enviada con éxito:', response.data);
        return response.data;
      } else {
        console.error('Error al enviar asistencia:', response.statusText);
      }
    } catch (error) {
      console.error('Error durante la solicitud POST:', error.message);
    }
  } catch (error) {
    console.error('Error al analizar los datos del usuario:', error.message);
  }
};


export const consulta = async (rut, tipo, entra, fechaDesde, fechaHasta) => {
  try {
    const userId = localStorage.getItem('user');

    if (!userId) {
      console.error('User data not found in localStorage');
      return;
    }

    const userObject = JSON.parse(userId);

    // Utiliza el _id del primer rol como accessToken
    const accessToken = userObject.roles.length > 0 ? userObject.roles[0]._id : null;

    if (!accessToken) {
      console.error('Access token not found in user roles');
      return;
    }

    try {
      // Construye la ruta con parámetros opcionales
      let ruta = `${API_URL}/asistencia/consulta/?`;

      if (rut !== null && rut !== undefined) {
        ruta += `rut=${rut}&`;
      }

      if (tipo !== null && tipo !== undefined) {
        ruta += `tipo=${tipo}&`;
      }

      if (entra !== null && entra !== undefined) {
        ruta += `entra=${entra}&`;
      }

      if (fechaDesde !== null && fechaDesde !== undefined) {
        ruta += `fechaDesde=${fechaDesde}&`;
      }

      if (fechaHasta !== null && fechaHasta !== undefined) {
        ruta += `fechaHasta=${fechaHasta}&`;
      }

      // Elimina el último '&'
      ruta = ruta.slice(0, -1);

      console.log("dsad"+ruta);

      const response = await axios.get(ruta, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
          'Content-Type': 'application/json',
        },
      });

      if (response.status === 200) {
        console.log('Consulta exitosa:', response.data);
        return response.data;
      } else {
        console.error('Error en la consulta:', response.statusText);
      }
    } catch (error) {
      console.error('Error durante la consulta GET:', error.message);
    }
  } catch (error) {
    console.error('Error al analizar los datos del usuario:', error.message);
  }
};

export const eliminarPersona = async (id) => {
  try {
    const userId = localStorage.getItem('user');

    if (!userId) {
      console.error('User data not found in localStorage');
      return;
    }

    const userObject = JSON.parse(userId);

    // Use the _id from the first role as the accessToken
    const accessToken = userObject.roles.length > 0 ? userObject.roles[0]._id : null;

    if (!accessToken) {
      console.error('Access token not found in user roles');
      return;
    }

    

    try {
      const ruta = `${API_URL}/asistencia/${id}`;

      const response = await axios.delete(ruta, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      });

      //console.log(response);

      if (response.status === 200) {
        const asistenciaData = response.data; // Cambio aquí: extrae la propiedad .data del response
        // Puedes manejar asistenciaData según sea necesario
        // console.log('Asistencia Data:', asistenciaData);
        return asistenciaData; // Cambio aquí: devuelve asistenciaData en lugar de response.data
      } else {
        console.error('Error fetching asistencia data:', response.statusText);
      }
    } catch (error) {
      console.error('Error fetching asistencia:', error.message);
    }
  } catch (error) {
    console.error('Error parsing user data:', error.message);
  }
};

export const eliminarRegistro = async (id,id_reg) => {
  try {
    const userId = localStorage.getItem('user');

    if (!userId) {
      console.error('User data not found in localStorage');
      return;
    }

    const userObject = JSON.parse(userId);

    // Use the _id from the first role as the accessToken
    const accessToken = userObject.roles.length > 0 ? userObject.roles[0]._id : null;

    if (!accessToken) {
      console.error('Access token not found in user roles');
      return;
    }

     

    try {
      const ruta = `${API_URL}/asistencia/${id}/${id_reg}`;

      const response = await axios.delete(ruta, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      });

      //console.log(response);

      if (response.status === 200) {
        const asistenciaData = response.data; // Cambio aquí: extrae la propiedad .data del response
        // Puedes manejar asistenciaData según sea necesario
        // console.log('Asistencia Data:', asistenciaData);
        return asistenciaData; // Cambio aquí: devuelve asistenciaData en lugar de response.data
      } else {
        console.error('Error fetching asistencia data:', response.statusText);
      }
    } catch (error) {
      console.error('Error fetching asistencia:', error.message);
    }
  } catch (error) {
    console.error('Error parsing user data:', error.message);
  }
};

export const actualizar = async (json) => {
  try {
    const userId = localStorage.getItem('user');

    if (!userId) {
      console.error('User data not found in localStorage');
      return;
    }

    const userObject = JSON.parse(userId);

    // Use the _id from the first role as the accessToken
    const accessToken = userObject.roles.length > 0 ? userObject.roles[0]._id : null;

    if (!accessToken) {
      console.error('Access token not found in user roles');
      return;
    }

     

    try {
      const ruta = `${API_URL}/asistencia/`;

      const response = await axios.put(ruta, json,{
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      });

      //console.log(response);

      if (response.status === 200) {
        const asistenciaData = response.data; // Cambio aquí: extrae la propiedad .data del response
        // Puedes manejar asistenciaData según sea necesario
        // console.log('Asistencia Data:', asistenciaData);
        return asistenciaData; // Cambio aquí: devuelve asistenciaData en lugar de response.data
      } else {
        console.error('Error fetching asistencia data:', response.statusText);
      }
    } catch (error) {
      console.error('Error fetching asistencia:', error.message);
    }
  } catch (error) {
    console.error('Error parsing user data:', error.message);
  }
};

